import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppComponent } from './app.component'
import { PostListComponent } from './post-list/post-list.component'
import { PostItemComponent } from './post-item/post-item.component'
import { RouterModule, Routes } from '@angular/router'
import { PostService } from './services/post.service'
import { NewPostComponent } from './new-post/new-post.component'
import { ReactiveFormsModule } from '@angular/forms'

const appRoutes: Routes = [
  { path: 'posts', component: PostListComponent },
  { path: 'new', component: NewPostComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostItemComponent,
    NewPostComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
  ],

  providers: [PostService],
  bootstrap: [AppComponent],
})
export class AppModule {}
