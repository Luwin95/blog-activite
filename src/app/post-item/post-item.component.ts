import { Component, OnInit, Input } from '@angular/core'
import { Post } from './../models/post.model'
import { PostService } from '../services/post.service'

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.scss'],
})
export class PostItemComponent implements OnInit {
  @Input() post: Post
  @Input() index: number

  constructor(private postService: PostService) {}

  onLike() {
    this.postService.addLike(this.index)
  }

  onDislike() {
    this.postService.removeLike(this.index)
  }

  onRemove() {
    this.postService.removePost(this.post)
  }

  ngOnInit() {}
}
