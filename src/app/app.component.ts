import { Component } from '@angular/core'
import * as firebase from 'firebase'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app'
  constructor() {
    const config = {
      apiKey: 'AIzaSyALncw6E2tdvK298v6WYHa1zOoE6s6NrLc',
      authDomain: 'activite-angular-ea76c.firebaseapp.com',
      databaseURL: 'https://activite-angular-ea76c.firebaseio.com',
      projectId: 'activite-angular-ea76c',
      storageBucket: 'activite-angular-ea76c.appspot.com',
      messagingSenderId: '724709374865',
    }
    firebase.initializeApp(config)
  }
}
