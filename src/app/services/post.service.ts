import { Injectable } from '@angular/core'
import { Post } from '../models/post.model'
import { Subject } from 'rxjs'
import * as firebase from 'firebase'

@Injectable()
export class PostService {
  postsSubject = new Subject<any[]>()

  private loremIpsum =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'

  posts: Post[] = []

  constructor() {
    this.getPosts()
  }

  emitPostSubject() {
    this.postsSubject.next(this.posts)
  }

  addLike(i: number) {
    this.posts[i].loveIts = this.posts[i].loveIts + 1
    this.savePosts()
    this.emitPostSubject()
  }

  removeLike(i: number) {
    this.posts[i].loveIts = this.posts[i].loveIts - 1
    this.savePosts()
    this.emitPostSubject()
  }

  addPost(post: Post) {
    if (this.posts) {
      post.id = this.posts[this.posts.length - 1].id + 1
    } else {
      post.id = 1
    }
    this.posts.push(post)
    this.savePosts()
    this.emitPostSubject()
  }

  removePost(post: Post) {
    const postIndexToRemove = this.posts.findIndex(postEl => {
      if (postEl === post) {
        return true
      }
    })
    this.posts.splice(postIndexToRemove, 1)
    this.savePosts()
    this.emitPostSubject()
  }

  savePosts() {
    firebase
      .database()
      .ref('/posts')
      .set(this.posts)
  }

  getPosts() {
    firebase
      .database()
      .ref('/posts')
      .on('value', (data: firebase.database.DataSnapshot) => {
        this.posts = data.val() ? data.val() : []
        this.emitPostSubject()
      })
  }
}
